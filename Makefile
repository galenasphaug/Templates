# Makefile
# Galen Asphaug <galenasphaug@gmail.com>

EXE = a.out
SRC = $(wildcard *.cpp) # Store all .cpp filenames in SRC
OBJ = $(src:.cpp=.o) # Store .o filenames in OBJ

CC=g++ # C++ compiler
CC_PARAMS=-Wall -pedantic -std=c++11 # Compiler flags

$(EXE): $(OBJ) # Executable
	$(CC) $(CC_PARAMS) $(OBJ) -o $@

#main.o: main.cpp
#	$(CC) $(CC_PARAMS) -c main.cpp -o $@

clean:
	rm -f a.out *.o *~ .*~
